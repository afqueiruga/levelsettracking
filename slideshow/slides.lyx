#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass beamer
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Digital Image Deformation Tracking via the Level Set Method
\end_layout

\begin_layout BeginFrame
Image Processing
\end_layout

\begin_layout Itemize
Tracking program written in Python using OpenCV and SciPy
\end_layout

\begin_layout Itemize
Custom levelset filter written in C
\end_layout

\begin_layout BeginFrame
The Level-Set Method
\end_layout

\begin_layout Standard
Want to solve operations on the boundary 
\begin_inset Formula $\mathbf{b}:\mathbb{R}\times\mathbb{R}\rightarrow\mathbb{R}^{2}$
\end_inset


\end_layout

\begin_layout Standard
Define the level set function 
\begin_inset Formula $\phi:\mathbb{R}^{2}\times\mathbb{R}\rightarrow\mathbb{R}$
\end_inset

 that represents the signed distance to the boundary
\begin_inset Formula 
\[
\phi\left(\mathbf{x},t\right)=\pm\min_{s}\left\Vert \mathbf{x}-\mathbf{b}\left(s,t\right)\right\Vert _{2}
\]

\end_inset

which is positive inside, negative outside the boundary.
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
centerline{
\end_layout

\end_inset


\begin_inset Graphics
	filename diagram.svg
	width 80text%

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout BeginFrame
Transforming into a PDE
\end_layout

\begin_layout Standard
We want to solve the ODE for points on the boundary
\begin_inset Formula 
\[
\frac{d\mathbf{b}}{dt}=\left(A-\epsilon\kappa\right)\mathbf{n}
\]

\end_inset

where 
\begin_inset Formula $\kappa$
\end_inset

 is the curvature and 
\begin_inset Formula $\mathbf{n}$
\end_inset

 is the unit normal of the surface.
 
\begin_inset Formula $A$
\end_inset

 controls evaporation, 
\begin_inset Formula $\epsilon$
\end_inset

 controls surface tension.
 This gives 
\begin_inset Quotes eld
\end_inset

bubbles
\begin_inset Quotes erd
\end_inset

 that grow, evaporate, and merge.
 
\end_layout

\begin_layout Standard
Using the level set method, the equation becomes a PDE
\begin_inset Formula 
\[
\frac{\partial\phi}{\partial t}+\left(A-\epsilon\kappa\right)\left|\nabla\phi\right|=0
\]

\end_inset

which can be solved by a number of numerical techniques, such as the Finite
 Difference Method.
\end_layout

\begin_layout BeginFrame
Winded Compact Finite Difference Scheme
\end_layout

\begin_layout Standard
The gradient and curvature are evaluated with central derivatives:
\begin_inset Formula 
\[
\left|\nabla\phi\right|=\sqrt{\phi_{x}^{2}+\phi_{y}^{2}}
\]

\end_inset


\begin_inset Formula 
\[
\kappa=\frac{\phi_{xx}\phi_{y}^{2}-2\phi_{x}\phi_{y}\phi_{xy}+\phi_{x}^{2}\phi_{yy}}{\left(\phi_{x}^{2}+\phi_{y}^{2}\right)^{\nicefrac{3}{2}}}.
\]

\end_inset


\end_layout

\begin_layout Standard
The flux function 
\begin_inset Formula $F\left\Vert \nabla\phi\right\Vert =\left(A-\epsilon\kappa\right)\left\Vert \nabla\phi\right\Vert $
\end_inset

 is evaluated by
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
F\left|\nabla\phi\right|=\max\left(F,0\right)\nabla_{ij}^{+}+\min\left(F,0\right)\nabla_{ij}^{-}
\]

\end_inset

where the winded gradient magnitudes are evaluated by
\begin_inset Formula 
\begin{eqnarray*}
\nabla_{ij}^{+}=\sqrt{\max\left(D_{x}^{-},0\right)^{2}+\min\left(D_{x}^{+},0\right)^{2}+\max\left(D_{y}^{-},0\right)^{2}+\min\left(D_{y}^{+},0\right)^{2}}\\
\nabla_{ij}^{-}=\sqrt{\min\left(D_{x}^{-},0\right)^{2}+\max\left(D_{x}^{+},0\right)^{2}+\min\left(D_{y}^{-},0\right)^{2}+\max\left(D_{y}^{+},0\right)^{2}}
\end{eqnarray*}

\end_inset

where 
\begin_inset Formula $D^{+}$
\end_inset

 and 
\begin_inset Formula $D^{-}$
\end_inset

 are the upwind and downwind derivatives.
\end_layout

\begin_layout BeginFrame
Image Processing with the Level Set
\end_layout

\begin_layout Standard
For image processing, the pixels are the points of the finite difference
 scheme.
\end_layout

\begin_layout Standard
Create a binary imager using a threshhold operation and create the levelset
 from the contour.
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
center{
\end_layout

\end_inset


\begin_inset Graphics
	filename binary.png
	lyxscale 30
	width 50text%

\end_inset


\begin_inset Graphics
	filename phi.png
	lyxscale 30
	width 50text%

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout BeginFrame
Using the Level Set to Track Features
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
center{
\end_layout

\end_inset


\begin_inset Graphics
	filename surfaceplot.png
	lyxscale 30
	width 80text%

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
The lines are ridges in the level set, and the intersections are maxima!
\end_layout

\begin_layout BeginFrame
Code outline
\end_layout

\begin_layout Enumerate
Open video and initialize point data 
\end_layout

\begin_layout Enumerate
For each frame:
\end_layout

\begin_deeper
\begin_layout Enumerate
Convert frame to gray scale and compute binary image adaptive threshold
\end_layout

\begin_layout Enumerate
Compute levelset function on contour
\end_layout

\begin_layout Enumerate
Apply curvature filter to levelset and recompute binary image
\end_layout

\begin_layout Enumerate
Recompute levelset on filtered binary image
\end_layout

\begin_layout Enumerate
For each point we are tracking:
\end_layout

\begin_deeper
\begin_layout Enumerate
Locate all the local maxima within a search radius
\end_layout

\begin_layout Enumerate
Pick the closest one as the new value
\end_layout

\end_deeper
\end_deeper
\begin_layout Enumerate
Save video of filters and dump history of points
\end_layout

\begin_layout BeginFrame
Results for Static Testing
\end_layout

\begin_layout Standard
Show movie
\end_layout

\begin_layout Standard
With multiple points, can compute strains
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
center{
\end_layout

\end_inset


\begin_inset Graphics
	filename static_yposition.png
	lyxscale 30
	width 50text%

\end_inset


\begin_inset Graphics
	filename static_strain.png
	lyxscale 30
	width 50text%

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout BeginFrame
Results for Baseball Shots
\end_layout

\begin_layout Standard
Show movie
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
center{
\end_layout

\end_inset


\begin_inset Graphics
	filename xposition.png
	lyxscale 30
	width 50text%

\end_inset


\begin_inset Graphics
	filename xvelocity.png
	lyxscale 30
	width 50text%

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
center{
\end_layout

\end_inset


\begin_inset Graphics
	filename xacceleration.png
	lyxscale 30
	width 50text%

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Differentiation adds noise to a signal, even after filtering
\end_layout

\begin_layout BeginFrame
Conclusions
\end_layout

\begin_layout Enumerate
Need a better camera with faster framerate and higher resolution
\end_layout

\begin_layout Enumerate
Implement more robust feature locator that can recover from getting lost
\end_layout

\begin_layout EndFrame

\end_layout

\end_body
\end_document
